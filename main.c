/* 
 * File:   main.c
 * Author: szczepanczykd
 *
 * Created on April 3, 2016, 11:28 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <time.h>
#include "DaemonConfig.h"
#include <getopt.h>
#include <signal.h>
/*
 * 
 */

void signal_handler(int signumber)
{
    if( signumber == SIGINT)
    {
    FILE *file = fopen("Error.log", "a");
        time_t t = time(NULL);
        struct tm tm = *localtime(&t);
        fprintf(file,"Reviced SIGINT! : %d-%d-%d %d:%d:%d\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
        fflush(file);
        fclose(file);
        exit(1);
    }
}

int checkdir(const char *filename)
{
      
    if(filename[strlen(filename) - 1] == '/')
    {
        return 1;
    }
    
    for(int i = strlen(filename) - 2;i>1;i--)
    {
        if(filename[i] == '.')
        { 
            return 0;
        }
        else if(filename[i - 1] == '/')
        {
            return 1;
        }
    }
    return 0;
}

void HelloWorldTask(FILE *file)
{
    while (1)
    { 
        time_t t = time(NULL);
        struct tm tm = *localtime(&t);
        fprintf(file,"Hello World ! : %d-%d-%d %d:%d:%d\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
        fflush(file);
        sleep(60);
    }
}

int main(int argc, char** argv) {

    FILE *file;
    pid_t process_id = 0;
    pid_t sid = 0;
    char *filedir = 0;
    int isBackground = 1;
       
    int a;
    const char * short_opt = "vf?l:";
    struct option long_opt[] =
    {
        {"version", no_argument, NULL,'v'},
        {"foreground", no_argument, NULL,'f'},
        {"logfile", required_argument, NULL,'l'},
        {"help", required_argument, NULL,'?'}
    };
    
    while ( (a = getopt_long(argc,argv,short_opt,long_opt,NULL)) != -1)
    {
        switch(a)
        {
            case 'v':
            printf("Program: %s Version: %d\n","SimpleDaemon",Daemon_VERSION_MAJOR);
            return (EXIT_SUCCESS); 
            break;
            
            case 'f':
                isBackground = 0;
            break;   
            case 'l':
            
                filedir = optarg;
                if(checkdir(filedir))
                {
                    printf("%s\n","Bad filedir schema. EXITING!");
                    return (1);
                }
                
            break;              
            case '?':
            printf("%s\n","Usage: daemon -v(--version) | -f(--foreground) | -l(--logfile) dir \n| -f(--foreground) -l(--logfile) dir: eg(/home/user/log.txt) \n| -l(--logfile) dir -f(--foreground)");
            break;              
                        
        };
    }
    
   
    if(isBackground)
        daemon(1,0);
  
    
    if (signal(SIGINT, signal_handler) == SIG_ERR)
    printf("\ncan't catch SIGINT\n");
    
    
    
    if(filedir)
    file = fopen(filedir, "w+");
    else
    file = fopen("log.txt", "w+");
    
            if(file)
            {
            HelloWorldTask(file);
            }
            else
            {
            printf("%s\n","Cannot open file! EXITING!");  
            return(1);
            }
      
    return (EXIT_SUCCESS); //unreachable

}


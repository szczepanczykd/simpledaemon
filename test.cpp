#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE Daemon
#include <boost/test/unit_test.hpp>
#include <cstring>

int checkdir(const char *filename)
{
      
    if(filename[strlen(filename) - 1] == '/')
    {
        return 1;
    }
    
    for(int i = strlen(filename) - 2;i>1;i--)
    {
        if(filename[i] == '.')
        { 
            return 0;
        }
        else if(filename[i - 1] == '/')
        {
            return 1;
        }
    }
    return 0;
}



BOOST_AUTO_TEST_CASE( checkDirTypical )
{
	BOOST_CHECK ( checkdir("/home/szczepanczykd/test.txt") == 0 );
}

BOOST_AUTO_TEST_CASE( checkDirBad )
{
        BOOST_CHECK ( checkdir("/home/szczepanczykd/") == 1 );
}

BOOST_AUTO_TEST_CASE( checkDirIncomplete )
{
        BOOST_CHECK ( checkdir("/home/szczepanczykd/t.") == 1 );
}
BOOST_AUTO_TEST_CASE( checkDirFailure )
{
        BOOST_CHECK ( checkdir("/home/szczepanczykd/test.txt") == 1 );
}

